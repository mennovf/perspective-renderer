#include "camera.hpp"
#include "utils.hpp"

Camera::Camera(){
    }

Camera::Camera(Mesh3D::Point pos, Mesh3D::Point d):
    Object3D(pos){
        dir(d);
    }

Eigen::Affine3d Camera::getViewTransform() const{
    Eigen::Vector3d axis = _dir.cross(Eigen::Vector3d::UnitZ());
    if (axis[0] || axis[1] || axis[2]){
        axis.normalize();
    }
    const auto amount = angle(_dir, Eigen::Vector3d::UnitZ());

    const auto translation = Eigen::Translation<double, 3>(-_pos);
    const auto rotation = Eigen::AngleAxis<double>(amount, axis);

    return rotation * translation;
}
