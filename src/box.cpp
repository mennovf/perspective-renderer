#include "box.hpp"
#include <array>
#include "mesh3D.hpp"
#include <utility>

void Box::generateMesh(){
    const int size = 4*3;
    Mesh3D mesh{size};
    const auto w = _dim.x/2.;
    const auto l = _dim.y/2.;
    const auto h = _dim.z/2.;

    std::array<Mesh3D::Point, 4> bot;
    bot[0] = Mesh3D::Point(w, l, -h);
    bot[1] = Mesh3D::Point(w, -l, -h);
    bot[2] = Mesh3D::Point(-w, -l, -h);
    bot[3] = Mesh3D::Point(-w, l, -h);

    std::array<Mesh3D::Point, 4> top;
    top[0] = Mesh3D::Point(w, l, h);
    top[1] = Mesh3D::Point(w, -l, h);
    top[2] = Mesh3D::Point(-w, -l, h);
    top[3] = Mesh3D::Point(-w, l, h);

    for (int i = 0; i<4 ; ++i){
        mesh.addEdge(bot[i], bot[(i+1)%4]);
        mesh.addEdge(top[i], top[(i+1)%4]);
        mesh.addEdge(bot[i], top[i]);
    }
    _mesh = std::move(mesh);
}
