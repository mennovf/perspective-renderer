#include "program.hpp"


Program::Program():
    _running{true},
    _sim{},
    _window{sf::VideoMode{640, 480}, "N-Body Simulation"}{
    }


void Program::run(){
    sf::Clock clock;
    sf::Event event;
    while(_running){
        _window.clear(sf::Color::Black);

        while(_window.pollEvent(event)){
            if(event.type == sf::Event::Closed){
                _running = false;
            }
            else{
                _sim.handleEvent(event);
            }
        }

        _sim.update(clock.restart());
        _sim.draw(_window);
        _window.display();
    }
}
