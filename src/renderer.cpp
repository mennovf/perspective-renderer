#include "renderer.hpp"

Renderer::Renderer():
    _camera{},
    _zoom{200.},
    _offset{}{
    }

Camera& Renderer::getCamera(){
    return _camera;
}

const Camera& Renderer::getCamera() const{
    return _camera;
}

void Renderer::setZoom(float z){
    _zoom = z;
}

const sf::Vector2f& Renderer::getOffset() const{
    return _offset;
}
void Renderer::setOffset(sf::Vector2f of){
    _offset = of;
}

void Renderer::renderMeshWireframe(sf::RenderTarget& target, Mesh3D& mesh) const{ // Note the NOT-constness of mesh
        const auto s = mesh.edges();

        auto vs = sf::VertexArray{sf::Lines, static_cast<unsigned int>(s * 2)}; //2 verts per edge of the

        mesh.transform(_camera.getViewTransform());
        for (int i = 0; i < s; ++i){
            const auto e = mesh.edge(i);
            
            const auto p1 = e.first;
            const auto p2 = e.second;

            //Don't display edges with verts behind the "screen"
            if (p1[2] < 1 || p2[2] < 1){
                continue;
            }

            //std::cout << "< " << p1.x << ", " << p1.y << ", " << p1.z << ">" << std::endl;
            const auto p1t = perspectiveTransform(p1);
            const auto p2t = perspectiveTransform(p2);

            vs[i*2] = sf::Vertex( _zoom*p1t + _offset, mesh._col);
            vs[i*2 + 1] = sf::Vertex( _zoom*p2t + _offset, mesh._col);
        }
        target.draw(vs);
}
