#include "utils.hpp"
#include <cmath>

Eigen::Vector3d toEigen(sf::Vector3<double> v){
    return {v.x, v.y, v.z};
}

sf::Vector3<double> toSFML(Eigen::Vector3d v){
    return {v[0], v[1], v[2]};
}

double angle(Eigen::Vector3d v1, Eigen::Vector3d v2){
    return std::acos(v1.normalized().dot(v2.normalized()));
}
