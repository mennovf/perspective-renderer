#include "cone.hpp"
#include <cmath>
#include "mesh3D.hpp"
#include <utility>


Cone::Cone (sf::Vector2<double> dim, int segs):
    _dim{dim},
    _segments{segs}{
        generateMesh();
}

void Cone::generateMesh(){
    const int size = 3 * _segments;
    Mesh3D mesh{size};

    const auto r = _dim.x;
    const auto h = _dim.y;

    const auto s = _segments;
    const auto top = Mesh3D::Point(0., 0., h);
    const auto origin = Mesh3D::Point(0., 0., 0.);

    Mesh3D::Point prev(r, 0., 0.);
    const auto angle = 2 * Mesh3D::PI / s;
    for (int i = 1; i <= s; ++i){
        Mesh3D::Point c;
        c[0] = r * std::cos(i*angle);
        c[1] = r * std::sin(i*angle);
        c[2] = 0;


        mesh.addEdge(c, prev);
        mesh.addEdge(c, top);
        mesh.addEdge(c, -origin);
        prev = c;
    }
    _mesh = std::move(mesh);
}
