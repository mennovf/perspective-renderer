#include "loadScene.hpp"
#include "sphere.hpp"
#include "cone.hpp"
#include "box.hpp"
#include "mesh3D.hpp"
#include "object3D.hpp"
#include <SFML/Graphics.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>


typedef std::unique_ptr<Object3D> ObjPtr;
std::vector<ObjPtr> loadScene(const std::string& filename){
    using boost::property_tree::ptree;
    std::vector<ObjPtr> objs;

    ptree pt;
    read_xml(filename, pt);

    BOOST_FOREACH(ptree::value_type const& v, pt.get_child("objects")){
        ObjPtr obj = nullptr;
        const auto c = v.second;
        const auto type = c.get<std::string>("type");


        //object agnostic lookup
        const auto px = c.get("pos.x", 0.);
        const auto py = c.get("pos.y", 0.);
        const auto pz = c.get("pos.z", 0.);
        const Mesh3D::Point pos{px, py, pz};

        const auto dx = c.get("dir.x", 0.);
        const auto dy = c.get("dir.y", 0.);
        const auto dz = c.get("dir.z", 1.);
        const Mesh3D::Point dir{dx, dy, dz};

        const auto rCol = c.get<unsigned char>("col.r", 255);
        const auto gCol = c.get<unsigned char>("col.g", 255);
        const auto bCol = c.get<unsigned char>("col.b", 255);
        const sf::Color col{rCol, gCol, bCol};

        //object type dependant lookup
        if(type == "sphere"){
            const auto r = c.get("r", 1.);
            const auto cs = c.get("cs", 20);
            const auto hs = c.get("hs", 20);
            obj = ObjPtr{new Sphere{r, sf::Vector2i{cs, hs}}};
        }
        if(type == "cone"){
            const auto r = c.get("r", 1.);
            const auto h = c.get("h", 2.);
            const auto segs = c.get("segs", 10);
            obj = ObjPtr{new Cone{sf::Vector2<double>{r, h}, segs}};
        }
        if(type == "box"){
            const auto w = c.get("w", 1.);
            const auto l = c.get("l", 1.);
            const auto h = c.get("h", 1.);
            obj = ObjPtr{new Box{sf::Vector3<double>{w, l, h}}};
        }

        obj->pos(pos);
        obj->dir(dir);
        obj->col(col);
        objs.emplace_back(std::move(obj));
    }
    return std::move(objs);
}
