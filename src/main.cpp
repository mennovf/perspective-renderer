#include <iostream>
#include "program.hpp"

int main(int argc, char const *argv[]){
    Program p;
    p.run();
    return 0;
}
