#include "mesh3D.hpp"
#include <utility>
#include <cassert>

Mesh3D::Mesh3D (const int es):
    _col{sf::Color::White},
    _edges{3, es*2},
    _current{0}
    {}

void Mesh3D::addEdge(Point fro, Point to){
    addEdge(std::make_pair(fro, to));
}

void Mesh3D::addEdge(Edge e){
    assert(_current < _edges.cols());

    _edges.col(_current) = e.first;
    _edges.col(_current+1) = e.second;
    _current += 2;
}

Mesh3D::Edge Mesh3D::edge(int i) const{
    return std::make_pair(_edges.col(i*2), _edges.col(i*2+1));
}

int Mesh3D::edges() const{
    return _edges.cols() / 2;
}

void Mesh3D::transform(const Eigen::Affine3d& t){
    _edges = t * _edges;
}
