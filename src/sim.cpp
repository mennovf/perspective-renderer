#include <cmath>
#include <Eigen/Geometry>

#include "sim.hpp"
#include "sphere.hpp"
#include "mesh3D.hpp"
#include "utils.hpp"
#include "sphere.hpp"
#include "cone.hpp"
#include "box.hpp"
#include "loadScene.hpp"
#include <iterator>

Sim::Sim():
    _objs{},
    _font{},
    _renderer{}{

    _renderer.getCamera().pos(Mesh3D::Point{0., 0., 30.});
    _renderer.setZoom(200.f);
    _renderer.setOffset(sf::Vector2f{320.f, 240.f});

    _font.loadFromFile("C:/Windows/Fonts/Arial.ttf");

    _objs.emplace_back(ObjPtr(new Sphere{10., sf::Vector2i{20, 20}}));

    _objs.emplace_back(ObjPtr(new Sphere{5., sf::Vector2i{20, 20}}));
    _objs.back()->pos(Mesh3D::Point{10., 10., 0.});

    _objs.emplace_back(ObjPtr(new Cone(sf::Vector2<double>{10., 20.}, 20)));
    _objs.back()->pos(Mesh3D::Point{0., -10.,0.});
    _objs.back()->dir(-Eigen::Vector3d::UnitY());

    _objs.emplace_back(ObjPtr(new Box{sf::Vector3<double>(10., 10., 10.)}));
    _objs.back()->pos(Mesh3D::Point{-10., 10., 0.});

    loadSceneFromFile("scene.xml");
}

void Sim::draw(sf::RenderTarget& target) const{
    for(const auto& obj : _objs){
        Mesh3D mesh = obj->getMesh();
        _renderer.renderMeshWireframe(target, mesh);
    }
    //Draw fps
    sf::Text text("FPS: " + to_string(getFps()), _font, 10);
    target.draw(text);
}

void Sim::handleScroll(sf::Event e){
    const auto rSpeed = 0.01;
    auto& s = _objs.front();
    const auto a = e.mouseWheel.delta;
    Eigen::Quaterniond r;
    if(a < 0){
        r = Eigen::Quaterniond(Eigen::AngleAxis<double>(a*rSpeed, Eigen::Vector3d::UnitY()));
    }else{
        r = Eigen::Quaterniond(Eigen::AngleAxis<double>(a*rSpeed, Eigen::Vector3d::UnitX()));
    }
    const Eigen::Vector3d newDir =  r * s->dir();
    s->dir(newDir);
}

void Sim::handleRightDrag(sf::Event e){
    const auto rSpeed = 0.01;
    const sf::Vector2i delta{e.mouseMove.x - prevMousePos.x, e.mouseMove.y - prevMousePos.y};
    Eigen::Affine3d r(Eigen::AngleAxis<double>(delta.x * rSpeed, Eigen::Vector3d::UnitY()));
    _renderer.getCamera().pos(r * _renderer.getCamera().pos());
}


void Sim::update(sf::Time dt){
    _renderer.getCamera().dir(-_renderer.getCamera().pos());
    //std::cout << _camera.dir() << std::endl << std::endl;
    //std::cout << _camera.pos() << std::endl << std::endl << std::endl;
}

void Sim::loadSceneFromFile(const std::string& filename){
    std::vector<ObjPtr> objs = loadScene(filename);

    _objs.insert(_objs.end(), std::make_move_iterator(objs.begin()), std::make_move_iterator(objs.end()));
}

double Sim::getFps() const{
    static sf::Clock c;
    return 1./c.restart().asSeconds();
}
