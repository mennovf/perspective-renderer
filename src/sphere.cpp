#include "sphere.hpp"
#include "mesh3D.hpp"
#include <cmath>
#include <SFML/System.hpp>
#include <vector>
#include <utility>
#include <memory>
#include <iostream>


Sphere::Sphere(double r, sf::Vector2i segments):
    _radius{r},
    _cs{segments.x},
    _hs{segments.y}
{
    generateMesh();
}

void Sphere::generateMesh(){
    const int size = _cs * _hs * 3; //Number of edges generated
    Mesh3D mesh{size};

    const double ca = 2*Mesh3D::PI/_cs; //circumference angle
    const double ha = Mesh3D::PI/_hs;
    const auto r = _radius;

    std::vector<Mesh3D::Point> prev(_cs+1, Mesh3D::Point{0.0, 0.0, -r});
    std::vector<Mesh3D::Point> current{_cs+1u};
    for(int i = 1; i <= _hs; ++i){
        const double phi = -Mesh3D::PI/2 + i*ha;

        for(int j = 1; j <= _cs+1; ++j){
            const double theta = j*ca;

            const double x = r*std::cos(phi)*std::cos(theta);
            const double y = r*std::cos(phi)*std::sin(theta);
            const double z = r*std::sin(phi);
            const auto p = Mesh3D::Point{x, y, z};
            current[j-1] = p;

            //_edges.push_back(std::make_pair(prev[j-1], p));
            mesh.addEdge(prev[j-1], p);
            if(j!=1){
                mesh.addEdge(current[j-2], p);
                //_edges.push_back(std::make_pair(current[j-2], p));
            }
        }
        prev = current;
    }
    _mesh = std::move(mesh);
}

void Sphere::radius(double r){
    _radius = r;
    generateMesh();
}

double Sphere::radius(){
    return _radius;
}
