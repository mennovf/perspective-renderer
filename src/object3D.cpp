#include "object3D.hpp"
#include <utility>
#include "utils.hpp"
#include <iostream>

Object3D::Object3D(Mesh3D::Point p):
    _pos{p},
    _dir{0.,0.,1.},
    _mesh{0}
    {
    }

Mesh3D Object3D::getMesh() const{
    Mesh3D newMesh = _mesh;

    newMesh.transform(getTransform());
    return newMesh;
}

void Object3D::pos(const Mesh3D::Point p){
    _pos = p;
}

Mesh3D::Point Object3D::pos() const{
    return _pos;
}

void Object3D::dir(const Mesh3D::Point d){
    _dir = d.normalized();
}

Mesh3D::Point Object3D::dir() const{
    return _dir;
}

Eigen::Affine3d Object3D::getTransform() const{
    Eigen::Vector3d axis = Eigen::Vector3d::UnitZ().cross(_dir.normalized());
    if (axis[0] || axis[1] || axis[2]){
        axis.normalize();
    }
    const auto amount = angle(_dir.normalized(), Eigen::Vector3d::UnitZ());

    Eigen::Affine3d transform(Eigen::AngleAxis<double>(amount, axis));
    return Eigen::Translation<double, 3>(_pos) * transform;
}

void Object3D::col(const sf::Color& c){
    _mesh._col = c;
}
