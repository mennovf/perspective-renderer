#ifndef PROGRAM_HPP
#define PROGRAM_HPP
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "sim.hpp"

class Program {
public:
    Program ();
    virtual ~Program () = default;

    void run();
protected:
    bool _running;
    Sim _sim;
    sf::RenderWindow _window;
};


#endif /* end of include guard: PROGRAM_HPP */

