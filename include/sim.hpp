#ifndef SIM_HPP
#define SIM_HPP
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <memory>

#include "eventClass.hpp"
#include "camera.hpp"
#include "object3D.hpp"
#include "renderer.hpp"

class Sim : public EventClass{
public:
    typedef std::unique_ptr<Object3D> ObjPtr;
    Sim ();
    virtual ~Sim () = default;

    void draw(sf::RenderTarget&) const;
    void update(sf::Time dt);
    virtual void handleScroll(sf::Event) override;
    virtual void handleRightDrag(sf::Event) override;

protected:
    std::vector<ObjPtr> _objs;
    sf::Font _font;
    Renderer _renderer;

    void loadSceneFromFile(const std::string&);
    double getFps() const;
};


#endif /* end of include guard: SIM_HPP */

