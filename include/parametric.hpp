#ifndef PARAMETRIC_HPP
#define PARAMETRIC_HPP
#include "object3D.hpp"
class Parametric : public Object3D{
public:
    Parametric() = default;
    virtual ~Parametric () = default;

protected:
    virtual void generateMesh() = 0;
};


#endif /* end of include guard: PARAMETRIC_HPP */

