#ifndef MESH3D_HPP
#define MESH3D_HPP
#include <utility>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <Eigen/Geometry>

class Mesh3D{
public:
    typedef Eigen::Matrix<double, 3, Eigen::Dynamic, Eigen::RowMajor> Edges;
    static constexpr double PI = 3.141592653589793238462;
    using Point = Eigen::Vector3d;
    using Edge = std::pair<Point, Point>;

    Mesh3D (const int);
    virtual ~Mesh3D () = default;

    void addEdge(Point, Point);
    void addEdge(Edge);
    Edge edge(int) const;

    int size() const{return edges();};
    int edges() const;

    void transform(const Eigen::Affine3d&);

    sf::Color _col;
protected:
    Edges _edges;
    int _current;
};


#endif /* end of include guard: MESH3D_HPP */

