#ifndef SPHERE_HPP
#define SPHERE_HPP
#include "parametric.hpp"

class Sphere : public Parametric{
public:
    Sphere(double r, sf::Vector2i segments = sf::Vector2i{5, 5});
    virtual ~Sphere() = default;

    void radius(double);
    double radius();

protected:
    double _radius;
    int _cs, _hs; //circumferece segments, height segments

    virtual void generateMesh() override;
};

#endif /* end of include guard: SPHERE_HPP */
