#ifndef CONE_HPP
#define CONE_HPP
#include "parametric.hpp"

class Cone : public Parametric{
public:
    Cone (sf::Vector2<double>, int);
    virtual ~Cone () = default;

protected:
    sf::Vector2<double> _dim; //radius and height
    int _segments;

    virtual void generateMesh() override;
};


#endif /* end of include guard: CONE_HPP */

