#ifndef OBJECT3D_HPP
#define OBJECT3D_HPP
#include "mesh3D.hpp"
#include <SFML/System.hpp>
#include <Eigen/Geometry>

class Object3D{
public:
    Object3D(Mesh3D::Point p = Mesh3D::Point{0.0, 0.0, 0.0});
    virtual ~Object3D () = default;

    Mesh3D getMesh() const;

    void pos(const Mesh3D::Point);
    Mesh3D::Point pos() const;

    void dir(const Mesh3D::Point);
    Mesh3D::Point dir() const;

    Eigen::Affine3d getTransform() const;
    void col(const sf::Color&);
protected:
    Mesh3D::Point _pos;
    Mesh3D::Point _dir;
    Mesh3D _mesh;

};


#endif /* end of include guard: OBJECT3D_HPP */

