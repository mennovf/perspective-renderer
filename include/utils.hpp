#ifndef UTILS_HPP
#define UTILS_HPP
#include <SFML/System.hpp>
#include <Eigen/Geometry>
#include <string>
#include <sstream>


Eigen::Vector3d toEigen(sf::Vector3<double>);
sf::Vector3<double> toSFML(Eigen::Vector3d);
double angle(Eigen::Vector3d, Eigen::Vector3d);

template<class T>
std::string to_string(const T& v){
    std::stringstream ss;
    ss << v;
    return ss.str();
}

#endif /* end of include guard: UTILS_HPP */

