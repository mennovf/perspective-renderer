#ifndef RENDERER_HPP
#define RENDERER_HPP
#include "Mesh3D.hpp"
#include "camera.hpp"
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

class Renderer{
public:
    Renderer ();
    virtual ~Renderer () = default;

    Camera& getCamera();
    const Camera& getCamera() const;

    void setZoom(float);
    const sf::Vector2f& getOffset() const;
    void setOffset(sf::Vector2f);
    void renderMeshWireframe(sf::RenderTarget&, Mesh3D&) const; // Note the NOT-constness of mesh

    inline sf::Vector2f perspectiveTransform(const Mesh3D::Point& p) const{
        return {static_cast<float>(p[0] / p[2]), static_cast<float>(p[1] / p[2])};
    }

protected:
    Camera _camera;
    float _zoom;
    sf::Vector2f _offset;
};


#endif /* end of include guard: RENDERER_HPP */

