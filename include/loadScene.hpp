#ifndef LOADSCENE_HPP
#define LOADSCENE_HPPLOADSCENE_HPP
#include <memory>
#include <string>
#include <vector>
#include "object3d.hpp"

std::vector<std::unique_ptr<Object3D>> loadScene(const std::string&);

#endif /* end of include guard: LOADSCENE_HPPLOADSCENE_HPP */

