#ifndef CAMERA_HPP
#define CAMERA_HPP
#include <Eigen/Geometry>
#include <SFML/System.hpp>
#include "object3D.hpp"

class Camera : public Object3D{
public:
    Camera ();
    Camera(Mesh3D::Point pos, Mesh3D::Point dir = Mesh3D::Point{0., 0., 1.});
    virtual ~Camera () = default;

    Eigen::Affine3d getViewTransform() const;
};


#endif /* end of include guard: CAMERA_HPP */

