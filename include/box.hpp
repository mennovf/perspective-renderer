#ifndef BOX_HPP
#define BOX_HPP
#include "parametric.hpp"
#include <SFML/System.hpp>

class Box : public Parametric{
public:
    Box (sf::Vector3<double> dim):
        _dim{dim}{
            generateMesh();
        }
    virtual ~Box () = default;

protected:
    sf::Vector3<double> _dim;
    virtual void generateMesh() override;
};


#endif /* end of include guard: BOX_HPP */

