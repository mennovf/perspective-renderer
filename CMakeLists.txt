cmake_minimum_required(VERSION 2.8)
project("Perspective Renderer")

set(EXECUTABLE_NAME Renderer)
#file(GLOB CPPS *.cpp)
set(CPPS
    ./src/main.cpp
    ./src/mesh3D.cpp
    ./src/object3D.cpp
    ./src/program.cpp
    ./src/sim.cpp
    ./src/sphere.cpp
    ./src/eventClass.cpp
    ./src/camera.cpp
    ./src/utils.cpp
    ./src/cone.cpp
    ./src/box.cpp
    ./src/loadScene.cpp
    ./src/renderer.cpp
    )

include_directories(./include)
add_executable(${EXECUTABLE_NAME} ${CPPS})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -pedantic -Weffc++")

if(CMAKE_BUILD_TYPE STREQUAL "")
    set(CMAKE_BUILD_TYPE Debug)
endif()
if (CMAKE_BUILD_TYPE STREQUAL Release)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mwindows")
endif()

#Detect and add boost
set(Boost_USE_STATIC_LIBS TRUE)
find_package(Boost REQUIRED)
if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIR})
endif()


set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules/" ${CMAKE_MODULE_PATH})
#Detect and add SFML
find_package(SFML 2 REQUIRED system window graphics audio)
if(SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(${EXECUTABLE_NAME} ${SFML_LIBRARIES})
endif()

target_link_libraries(${EXECUTABLE_NAME} ${SFML_LIBRARIES})
include_directories(${SFML_INCLUDE_DIR})
#file(COPY "${CMAKE_SOURCE_DIR}/rsc" DESTINATION "${CMAKE_BINARY_DIR}")
